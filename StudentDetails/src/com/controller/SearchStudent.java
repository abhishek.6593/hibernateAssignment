package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.dto.StudentDTO;
import com.model.Student;

public class SearchStudent extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, 
			HttpServletResponse resp)
					throws ServletException, IOException {

		resp.setContentType("text/html");
		PrintWriter out=resp.getWriter();

		//get the data from the form(HTML)
		String sid=req.getParameter("sid");

		StudentDAO dao=new StudentDAO();

		List<Student> std=dao.searchStudent(Integer.parseInt(sid));

		if(std==null)
		{
			out.println("Std Id is not found inn db");

		}else{
			for (Student s : std) {

				out.println("The sid is :"+s.getStudId());
				out.println("The name is :"+s.getStudentName());
				out.println("The address is :"+s.getAdd());

			}
		}//end if-else

	}//end of doGet

}//end of class
