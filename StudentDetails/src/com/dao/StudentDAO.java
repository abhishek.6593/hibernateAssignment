package com.dao;

import java.util.List;



import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.query.Query;

import com.dto.StudentDTO;
import com.model.Student;

public class StudentDAO {

	public List<Student> searchStudent(int a){

		Configuration config = new Configuration();
		config.configure("hibernate.cfg.xml");
		
		SessionFactory factory = config.buildSessionFactory();
		
		Session session =factory.openSession();
		Transaction tr = session.beginTransaction();

		Query query = session.createQuery("from Student where studId =:id");

		query.setParameter("id",a);

		List<Student> std = query.list();
		if(std!=null)
		{
			return std;
		}else{
			return null;
		}

	}//end of method
}//end of class
